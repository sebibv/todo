<?php

use Illuminate\Database\Seeder;

class PlannersTableSeeder extends Seeder

{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        for($i=0;$i<30;$i++){

            DB::table('planners')->insert([

                'name' => $faker->name,
                'user_id' => $faker->numberBetween(1,30),
            ]);
        }
    }
}
