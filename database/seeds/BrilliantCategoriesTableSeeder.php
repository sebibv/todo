<?php

use Illuminate\Database\Seeder;

class BrilliantCategoriesTableSeeder extends Seeder

{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        for($i=0;$i<30;$i++){

            DB::table('brilliant_categories')->insert([

                'category_name' => $faker->name(),
            ]);
        }
    }
}
