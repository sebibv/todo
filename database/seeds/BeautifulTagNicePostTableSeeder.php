<?php

use Illuminate\Database\Seeder;

class BeautifulTagNicePostTableSeeder extends Seeder

{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        for($i=0;$i<30;$i++){

            DB::table('beautiful_tag_nice_post')->insert([

                'beautiful_tag_id' => $faker->numberBetween(1,30),
                'nice_post_id' => $faker->numberBetween(1,30),
                'priority' => $faker->numberBetween(1,10),
                'note' => $faker->name(),
            ]);
        }
    }
}
