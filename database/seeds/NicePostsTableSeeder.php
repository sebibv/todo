<?php

use Illuminate\Database\Seeder;

class NicePostsTableSeeder extends Seeder

{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        for($i=0;$i<30;$i++){

            DB::table('nice_posts')->insert([

                'big_title' => $faker->name(),
                'shallow_body' => $faker->paragraph(),
                'brilliant_category_id' => $faker->numberBetween(1,30),
            ]);
        }
    }
}
