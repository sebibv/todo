<?php

use Illuminate\Database\Seeder;

class TagsTableSeeder extends Seeder

{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        for($i=0;$i<30;$i++){

            DB::table('tags')->insert([

                'planner_id' => $faker->numberBetween(1,30),
                'tag' => $faker->text,
            ]);
        }
    }
}
