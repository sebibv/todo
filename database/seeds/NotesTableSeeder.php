<?php

use Illuminate\Database\Seeder;

class NotesTableSeeder extends Seeder

{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        for($i=0;$i<30;$i++){

            DB::table('notes')->insert([

                'planner_id' => $faker->numberBetween(1,30),
                'note' => $faker->text,
            ]);
        }
    }
}
