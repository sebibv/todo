<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(NicePostsTableSeeder::class);
        $this->call(BeautifulTagsTableSeeder::class);
        $this->call(BrilliantCategoriesTableSeeder::class);
        $this->call(GoodCommentsTableSeeder::class);
        $this->call(BeautifulTagNicePostTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(PlannersTableSeeder::class);
        $this->call(NotesTableSeeder::class);
        $this->call(TagsTableSeeder::class);
        $this->call(RemindersTableSeeder::class);
    }
}
