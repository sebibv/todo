<?php

use Illuminate\Database\Seeder;

class GoodCommentsTableSeeder extends Seeder

{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        for($i=0;$i<30;$i++){

            DB::table('good_comments')->insert([

                'super_title' => $faker->name(),
                'eligible_body' => $faker->paragraph(),
                'nice_post_id' => $faker->numberBetween(1,30),
            ]);
        }
    }
}
