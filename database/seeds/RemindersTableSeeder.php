<?php

use Illuminate\Database\Seeder;

class RemindersTableSeeder extends Seeder

{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        for($i=0;$i<30;$i++){

            DB::table('reminders')->insert([

                'note_id' => $faker->numberBetween(1,30),
                'when' => $faker->dateTime,
            ]);
        }
    }
}
