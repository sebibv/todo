<?php

use Illuminate\Database\Seeder;

class BeautifulTagsTableSeeder extends Seeder

{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        for($i=0;$i<30;$i++){

            DB::table('beautiful_tags')->insert([

                'tag_name' => $faker->name(),
            ]);
        }
    }
}
