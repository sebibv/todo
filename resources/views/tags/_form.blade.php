
<div class="form-group">
  <label for="tag-field">Tag</label>

  <input type="text" class="form-control
  @if($errors->any()) @if($errors->has("model.tag")) is-invalid @else is-valid @endif @endif;
  " id="tag-field" name="model[tag]" value="@if($errors->any()){{ old('model.tag') }}@else{{ $tag->tag or '' }}@endif" required>

  @if($errors->has("model.tag"))
    <div class="invalid-feedback">{{ $errors->first("model.tag") }}</div>
  @else
    <div class="invalid-feedback">Invalid!</div>
  @endif
</div>






<div class="form-group">
  <label for="planner_id-field">Planner</label>

  <select class="form-control
  @if($errors->any()) @if($errors->has("model.planner_id")) is-invalid @else is-valid @endif @endif
  " id="planner_id-field" name="model[planner_id]">
    <option value=""></option>
  @foreach( $lists["Planner"] as $list_key => $list_item )
    <option value="{{ $list_key }}"
      @if($errors->any())
        @if( old('planner_id') == $list_key ) selected='selected' @endif
      @else
        @if( isset($tag) && $tag->planner_id==$list_key )  selected='selected' @endif
      @endif
   >{{ $list_item }}</option>
  @endforeach
  </select>
  @if($errors->has("model.planner_id"))
    <div class="invalid-feedback">{{ $errors->first("model.planner_id") }}</div>
  @else
    <div class="invalid-feedback">Invalid!</div>
  @endif
</div>




