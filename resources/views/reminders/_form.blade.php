
<div class="form-group">
  <label for="when-field">When?</label>

  <input type="datetime-local" class="form-control
  @if($errors->any())
    @if($errors->has("model.when"))
      is-invalid
    @else
      is-valid
    @endif
  @endif;
  " id="when-field" name="model[when]" value="@if($errors->any()){{ old('model.when') }}@else{{ $reminder->when or '' }}@endif" required>

  @if($errors->has("model.when"))
    <div class="invalid-feedback">{{ $errors->first("model.when") }}</div>
  @else
    <div class="invalid-feedback">Invalid!</div>
  @endif
</div>






<div class="form-group">
  <label for="note_id-field">Note</label>

  <select class="form-control
  @if($errors->any()) @if($errors->has("model.note_id")) is-invalid @else is-valid @endif @endif
  " id="note_id-field" name="model[note_id]">
    <option value=""></option>
  @foreach( $lists["Note"] as $list_key => $list_item )
    <option value="{{ $list_key }}"
      @if($errors->any())
        @if( old('note_id') == $list_key ) selected='selected' @endif
      @else
        @if( isset($reminder) && $reminder->note_id==$list_key )  selected='selected' @endif
      @endif
   >{{ $list_item }}</option>
  @endforeach
  </select>
  @if($errors->has("model.note_id"))
    <div class="invalid-feedback">{{ $errors->first("model.note_id") }}</div>
  @else
    <div class="invalid-feedback">Invalid!</div>
  @endif
</div>




