<div class="row">
    <div class="col-md-12">

        @if($planners->count())

            <table class="table table-sm table-striped sp-omit">
              <thead>
                <tr>
                  <th scope="col"><div class="d-flex">
                    @if( method_exists($planners, 'appends') )
                      <a href="javascript:sortByColumn('id')">ID</a>
                      @if( Request::input('q.s') == 'id_asc' )<i class="material-icons">arrow_drop_up</i>
                      @elseif( Request::input('q.s') == 'id_desc' )<i class="material-icons">arrow_drop_down</i> @endif
                    @else
                      ID
                    @endif
                  </div></th>
                  <th scope="col"><div class="d-flex">
                    @if( method_exists($planners, 'appends') )
                      <a href="javascript:sortByColumn('name')">Name Of Planner</a>
                      @if( Request::input('q.s') == 'name_asc' )<i class="material-icons">arrow_drop_up</i>
                      @elseif( Request::input('q.s') == 'name_desc' )<i class="material-icons">arrow_drop_down</i> @endif
                    @else
                      Name Of Planner                    @endif
                  </div></th>

                  <th scope="col">USER</th>

                  <th scope="col">Note</th>
                  <th scope="col">Tag</th>


                  <th class="text-right" scope="col">OPTIONS</th>
                </tr>
              </thead>
              <tbody>
                @foreach($planners as $planner)
                    <tr>
                      <td scope="row"><a href="{{ route('planners.show', $planner->id) }}">{{$planner->id}}</a></td>
                      <td><a href="{{ route('planners.notes.index', $planner->id) }}" >{{$planner->name}}</a></td>

                      <td>@if($planner->user)<a href="{{ route('users.show', $planner->user->id) }}">{{ $planner->user->name }}</a>@else - @endif</td>

                      <td>
                          @foreach($planner->notes as $note)
                                        @if (!$loop->first) , @endif
                                        <a href="{{ route('planners.notes.show', [$planner->id, $note->id]) }}">{{ $note->note }}</a>
                          @endforeach
                      </td>
                      <td>
                          @foreach($planner->tags as $tag)
                                        @if (!$loop->first) , @endif
                                        <a href="{{ route('tags.show', $tag->id) }}">{{ $tag->tag }}</a>
                          @endforeach
                      </td>


                      <td class="text-right">
                        <div class="btn-group" role="group">
                            <a class="btn btn-sm btn-primary" href="{{ route('planners.notes.create', $planner->id) }}" data-toggle="tooltip" data-placement="top" title="Add Note to Planner"><i class="material-icons d-block">note_add</i></a>
                            <a class="btn btn-sm btn-warning" href="{{ route('planners.edit', $planner->id) }}" data-toggle="tooltip" data-placement="top" title="Edit Planner"><i class="material-icons d-block">edit</i></a>
                            <form method="POST" action="{{ route('planners.destroy', $planner->id) }}" accept-charset="UTF-8" style="display: inline;" onsubmit="if(confirm('Delete Planner? Are you sure?')) { return true } else {return false };">
                                {{ method_field('DELETE') }}
                                {{ csrf_field() }}
                                <button type="submit" class="btn btn-sm btn-danger" data-toggle="tooltip" data-placement="top" title="Delete Planner"><i class="material-icons d-block">delete</i></button>
                            </form>
                        </div>
                      </td>
                    </tr>
                @endforeach
              </tbody>
            </table>
            @if( method_exists($planners, 'appends') )
              {!! $planners->appends(Request::except('page'))->render() !!}
            @endif
        @else
            <h3 class="text-center alert alert-info">Empty!</h3>
        @endif
    </div>
</div>
