
<div class="form-group">
  <label for="note-field">Nota</label>

  <textarea class="form-control
  @if($errors->any()) @if($errors->has("body.note")) is-invalid @else is-valid @endif @endif;
  " id="note-field" name="body[note]" rows="3" required>@if($errors->any()){{ old('body.note') }}@else{{ $note->note or '' }}@endif</textarea>

  @if($errors->has("body.note"))
    <div class="invalid-feedback">{{ $errors->first("body.note") }}</div>
  @else
    <div class="invalid-feedback">Invalid!</div>
  @endif
</div>




{{-- <p>Uhu: {{ var_dump($lists) }}</p> --}}

{{-- <div class="form-group">
  <label for="planner_id-field">Planner</label>
  <select class="form-control
  @if($errors->any()) @if($errors->has("model.planner_id")) is-invalid @else is-valid @endif @endif
  " id="planner_id-field" name="model[planner_id]">
    <option value=""></option>
  @foreach( $lists["Planner"] as $list_key => $list_item )
    <option value="{{ $list_key }}"
      @if($errors->any())
        @if( old('planner_id') == $list_key ) selected='selected' @endif
      @else
        @if( isset($note) && $note->planner_id==$list_key )  selected='selected' @endif
      @endif
   >{{ $list_item }}</option>
  @endforeach
  </select>
  @if($errors->has("model.planner_id"))
    <div class="invalid-feedback">{{ $errors->first("model.planner_id") }}</div>
  @else
    <div class="invalid-feedback">Invalid!</div>
  @endif
</div> --}}




