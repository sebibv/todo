<div class="row">
    <div class="col-md-12">

        @if($planner->notes->count())

            <table class="table table-sm table-striped sp-omit">
              <thead>
                <tr>
                  <th scope="col"><div class="d-flex">
                    @if( method_exists($planner->notes, 'appends') )
                      <a href="javascript:sortByColumn('id')">ID</a>
                      @if( Request::input('q.s') == 'id_asc' )<i class="material-icons">arrow_drop_up</i>
                      @elseif( Request::input('q.s') == 'id_desc' )<i class="material-icons">arrow_drop_down</i> @endif
                    @else
                      ID
                    @endif
                  </div></th>
                  <th scope="col"><div class="d-flex">
                    @if( method_exists($planner->notes, 'appends') )
                      <a href="javascript:sortByColumn('note')">Nota</a>
                      @if( Request::input('q.s') == 'note_asc' )<i class="material-icons">arrow_drop_up</i>
                      @elseif( Request::input('q.s') == 'note_desc' )<i class="material-icons">arrow_drop_down</i> @endif
                    @else
                      Nota                    @endif
                  </div></th>

                  <th scope="col">Planner</th>

                  <th scope="col">Reminder</th>


                  <th class="text-right" scope="col">OPTIONS</th>
                </tr>
              </thead>
              <tbody>
                @foreach($planner->notes as $note)
                    <tr>
                      <td scope="row"><a href="{{ route('planners.notes.show', [$note->planner->id, $note->id]) }}">{{$note->id}}</a></td>
                      <td>{{$note->note}}</td>

                      <td>@if($note->planner)<a href="{{ route('planners.show', $note->planner->id) }}">{{ $note->planner->name }}</a>@else - @endif</td>

                      <td>
                          @foreach($note->reminders as $reminder)
                                        @if (!$loop->first) , @endif
                                        <a href="{{ route('reminders.show', $reminder->id) }}">{{ $reminder->when }}</a>
                          @endforeach
                      </td>


                      <td class="text-right">
                        <div class="btn-group" role="group">
                            {{-- <a class="btn btn-sm btn-primary" href="{{ route('notes.duplicate', $note->id) }}" data-toggle="tooltip" data-placement="top" title="Duplicate"><i class="material-icons d-block">add_to_photos</i></a> --}}
                            <a class="btn btn-sm btn-warning" href="{{ route('planners.notes.edit', [$planner->id, $note->id]) }}" data-toggle="tooltip" data-placement="top" title="Edit"><i class="material-icons d-block">edit</i></a>
                            <form method="POST" action="{{ route('planners.notes.destroy', [$planner->id, $note->id]) }}" accept-charset="UTF-8" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
                                {{ method_field('DELETE') }}
                                {{ csrf_field() }}
                                <button type="submit" class="btn btn-sm btn-danger" data-toggle="tooltip" data-placement="top" title="Delete"><i class="material-icons d-block">delete</i></button>
                            </form>
                        </div>
                      </td>
                    </tr>
                @endforeach
              </tbody>
            </table>
            @if( method_exists($planner->notes, 'appends') )
              {!! $planner->notes->appends(Request::except('page'))->render() !!}
            @endif
        @else
            <h3 class="text-center alert alert-info">Empty!</h3>
        @endif
    </div>
</div>
