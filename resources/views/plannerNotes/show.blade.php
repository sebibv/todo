@extends('layout')

@section('breadcrumb')
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="/" class="d-inline-flex"><i class="material-icons mr-1">home</i> Home</a></li>
        <li class="breadcrumb-item"><a href="{{ route('planners.notes.index', $plannerId) }}">{{ $model_title_list['notes'] }}</a></li>
        <li class="breadcrumb-item active">#{{ $note->id }}</li>
      </ol>
    </nav>
@endsection


@section('content')

@section('content')
    <h3>Planner id: {{ $plannerId }}</h3>
    <h1 class="d-flex mb-3">
        <i class="material-icons align-self-center mr-1">subject</i>
        <span class="d-inline-block">{{ $model_title_list['notes'] }} / Show #{{$note->id}}</span>
        <form class="ml-auto" method="POST" action="{{ route('notes.destroy', $note->id) }}" accept-charset="UTF-8" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
            {{ method_field('DELETE') }}
            {{ csrf_field() }}
            <div class="btn-group" role="group">
{{--                 <a class="btn btn-sm btn-primary" href="{{ route('planners.notes.duplicate', [$plannerId, $note->id]) }}" data-toggle="tooltip" data-placement="top" title="Duplicate"><i class="material-icons d-block">add_to_photos</i></a> --}}
                <a class="btn btn-sm btn-warning" href="{{ route('planners.notes.edit', [$plannerId, $note->id]) }}" data-toggle="tooltip" data-placement="top" title="Edit"><i class="material-icons d-block">edit</i></a>
                <button type="submit" class="btn btn-sm btn-danger" data-toggle="tooltip" data-placement="top" title="Delete"><i class="material-icons d-block">delete</i></button>
            </div>
        </form>
    </h1>

    <ul class="list-group list-group-flush mt-4">
      <li class="list-group-item d-inline-flex flex-wrap"><div><strong>ID ： </strong></div><div>{{$note->id}}</div></li>

      <li class="list-group-item d-inline-flex flex-wrap"><div><strong>Nota : </strong></div><div>{{ $note->note }}</div></li>

      <li class="list-group-item d-inline-flex flex-wrap"><div><strong>Planner : </strong></div><div>{{ $note->planner->name or '' }}</div></li>

      <li class="list-group-item"><p><strong>Reminder : </strong></p><div>
        @include('plannerNotes._tableReminders', ['reminders' => $note->reminders])
      </div></li>
      <li class="list-group-item"><p><strong>Reminder Add: </strong></p><div>
        <form method="POST" action="/reminders" class="needs-validation form-inline custom-form">
            @csrf
            <input type="hidden" name="model[note_id]" value="{{$note->id}}">
            <div class="form-group">
              <label for="when-field">When?</label>
              <input class="form-control" type="datetime-local" class="form-control" id='when-field' name="model[when]" value="" required>
            </div>
            <div class="d-flex justify-content-end">
              <button type="submit" class="btn btn-primary">Create</button>
            </div>
        </form>
      </div></li>


    </ul>
    <div class="d-flex justify-content-end mt-3">
        <a class="btn btn-secondary d-inline-flex mr-3" href="{{ route('planners.notes.index', $plannerId) }}"><i class="material-icons">fast_rewind</i> Back</a>
    </div>

@endsection

@endsection
