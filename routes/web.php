<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});
// Route::get("nicePosts/{nicePost}/duplicate", ['as' => 'nicePosts.duplicate', 'uses' => 'NicePostController@duplicate']);
// Route::resource("nicePosts","NicePostController");
// Route::get("beautifulTags/{beautifulTag}/duplicate", ['as' => 'beautifulTags.duplicate', 'uses' => 'BeautifulTagController@duplicate']);
// Route::resource("beautifulTags","BeautifulTagController");
// Route::get("brilliantCategories/{brilliantCategory}/duplicate", ['as' => 'brilliantCategories.duplicate', 'uses' => 'BrilliantCategoryController@duplicate']);
// Route::resource("brilliantCategories","BrilliantCategoryController");
// Route::get("goodComments/{goodComment}/duplicate", ['as' => 'goodComments.duplicate', 'uses' => 'GoodCommentController@duplicate']);
// Route::resource("goodComments","GoodCommentController");

Auth::routes();

Route::get('/', function () { return view('auth.login'); })->middleware('guest');


Route::get('/home', 'HomeController@index')->name('home');

Route::get("users/{user}/duplicate", ['as' => 'users.duplicate', 'uses' => 'UserController@duplicate']);

Route::resource("users","UserController");

Route::get("planners/{planner}/duplicate",
    ['as' => 'planners.duplicate', 'uses' => 'PlannerController@duplicate']);
Route::resource("planners","PlannersController");

Route::get("users/{user}/duplicate",
    ['as' => 'users.duplicate', 'uses' => 'UserController@duplicate']);
Route::resource("users","UserController");
Route::get("planners/{planner}/duplicate",
    ['as' => 'planners.duplicate', 'uses' => 'PlannerController@duplicate']);
Route::resource("planners","PlannerController");
Route::get("notes/{note}/duplicate",
    ['as' => 'notes.duplicate', 'uses' => 'NoteController@duplicate']);
Route::resource("notes","NoteController");
Route::get("tags/{tag}/duplicate",
    ['as' => 'tags.duplicate', 'uses' => 'TagController@duplicate']);
Route::resource("tags","TagController");
Route::get("reminders/{reminder}/duplicate",
    ['as' => 'reminders.duplicate', 'uses' => 'ReminderController@duplicate']);
Route::resource("reminders","ReminderController");

Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

Route::resource('planners.notes', 'PlannerNoteController');
// Route::resource('notes.reminders', 'RemainderController');
// Route::get('/planners/{planner}/notes/create', 'NoteController@create');
