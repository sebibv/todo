<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Reminder extends Model
{
    use SoftDeletes;


	// Mass Assignment
	protected $fillable = ['note_id','when'];
    protected $dates = ['deleted_at','when'];


	// Validate Rule
    public static function getValidateRule(Reminder $reminder=null){
        if($reminder){
            $ignore_unique = $reminder->id;
        }else{
            $ignore_unique = 'NULL';
        }
        $table_name = 'reminders';
        $validation_rule = [

            'model.note_id' => 'integer|nullable',
            'model.when' => 'date',


        ];
        if($reminder){

        }
        return $validation_rule;
    }



	public function note() {
		return $this->belongsTo('App\Note');
	}

    protected $casts = [
        // ia din baza de date si transforma, ex: 0 sau 1 in true sau false
        // 'when' => 'datetime:Y-m-d H:i:00'
    ];


	public static function getLists() {
		$lists = [];
		$lists['Note'] = Note::pluck( 'note' ,'id' );
		return $lists;
	}
}
