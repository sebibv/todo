<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

// use App\User;
class Planner extends Model
{

    public $rules = [
        'user_id' => 'required',
        'name' => 'required',
    ];
	// Mass Assignment
	protected $fillable = ['name','user_id',];


	// Validate Rule
    public static function getValidateRule(Planner $planner=null){
        if($planner){
            $ignore_unique = $planner->id;
        }else{
            $ignore_unique = 'NULL';
        }
        $table_name = 'planners';
        $validation_rule = [

            'model.name' => 'string',
            'model.user_id' => 'integer',

        ];
        if($planner){

        }
        return $validation_rule;
    }

	public function notes() {
		return $this->hasMany('App\Note');
	}
	public function tags() {
		return $this->hasMany('App\Tag');
	}
	public function user() {
		return $this->belongsTo('App\User');
	}

    public function addNote($note) {
        $this->notes()->create($note);
    }

    public function updateNote($note, $noteId) {
        // \Log::info($note);
        $this->notes()->where('id', '=', $noteId)->update($note);
    }
    //PDO fix
    protected $casts = [
        'user_id' => 'integer'
    ];

	public static function getLists() {
		$lists = [];
		$lists['User'] = User::pluck( 'name' ,'id' );
		$lists['Note'] = Note::pluck( 'note' ,'id' );
		$lists['Tag'] = Tag::pluck( 'tag' ,'id' );
		return $lists;
	}
}
