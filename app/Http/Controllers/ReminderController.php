<?php

namespace App\Http\Controllers;

use App\Reminder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Storage;
use Illuminate\Console\Scheduling\Schedule;
use \App\Jobs\ProcessReminders;
use Illuminate\Support\Facades\Auth;

// use Illuminate\Console\Scheduling\Schedule;
// use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class ReminderController extends Controller
{

    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
		$reminders = new Reminder;

        // (1)filltering
        if( is_array($request->input('q')) ){

            foreach( $request->input('q') as $key => $value ){

                if($key !== 's'){

                    $pettern = '#([^\.]*)\.?([^\.]*)_([^\._]*)#u';
                    preg_match($pettern,$key,$m);

                    $collumn_name = $m[1];
                    $related_column_name = $m[2];

                    if($m[3] == 'eq'){
                        $operator = '=';
                    }elseif($m[3] == 'cont'){
                        $operator = 'like';
                        $value = '%'.$value.'%';
                    }elseif($m[3] == 'gt'){
                        $operator = '>=';
                    }elseif($m[3] == 'lt'){
                        $operator = '<=';
                    }

                    if( $related_column_name !== '' ){  // search at related table column

                        $reminders = $reminders->whereHas($collumn_name, function($q) use($related_column_name, $operator, $value){
    						$q->where( $related_column_name, $operator, $value );
                        });

                    }else{
                        $reminders = $reminders->where( $collumn_name, $operator, $value );
                    }
                }
            }
        }
        $reminders = $reminders->get();



        // (2)sort
        $q_s = $request->input('q.s');
        if($q_s){

            // sort dir and sort column
            if( substr( $q_s,-5,5 ) === '_desc' ){
                $sort_column = substr( $q_s, 0, strlen($q_s)-5 );
                $reminders = $reminders->sortByDesc($sort_column);
            }elseif( substr( $q_s,-4,4 ) === '_asc' ){
                $sort_column = substr( $q_s, 0, strlen($q_s)-4 );
                $reminders = $reminders->sortBy($sort_column);
            }

        }else{
            $reminders = $reminders->sortByDesc('id');
        }



        // (3)paginate
        $reminders = $reminders->paginate(10);

		return view('reminders.index', compact('reminders'));
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('reminders.create')->with( 'lists', Reminder::getLists() );
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate_custom($request);
        // var_dump($request->all());
        $input = $request->input('model');
        // var_dump($input);
        $userTime =  str_replace("T", " ", $input['when']) . ':00';
        // var_dump($userTime);
        $timeToAlert = \Carbon\Carbon::createFromFormat(
            'Y-m-d H:i:s',
            $userTime, auth()->user()->timezone
        )->setTimezone('UTC');
        $input['when'] = $timeToAlert;
        // var_dump($timeToAlert);
        // var_dump($input);


        DB::beginTransaction();


		//create data
		$reminder = Reminder::create( $input );

        //sync(attach/detach)
        if($request->input('pivots')){
            $this->sync($request->input('pivots'), $reminder);
        }

        DB::commit();

        return back()->with('message', 'Item created successfully.');
		// return redirect()->route('reminders.index')->with('message', 'Item created successfully.');
    }



    /**
     * Display the specified resource.
     *
     * @param  \App\Reminder  $reminder     * @return \Illuminate\Http\Response
     */
    public function show(Reminder $reminder)
    {
        // \Debugbar::info('show');
        // $schedule = new Schedule;
        // $this->reminderAt($schedule);
		return view('reminders.show', compact('reminder'));
    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Reminder  $reminder     * @return \Illuminate\Http\Response
     */
    public function edit(Reminder $reminder)
    {
		return view('reminders.edit', compact('reminder'))->with( 'lists', Reminder::getLists() );
    }



	/**
	 * Show the form for duplicatting the specified resource.
	 *
	 * @param \App\Reminder  $reminder	 * @return \Illuminate\Http\Response
	 */
	public function duplicate(Reminder $reminder)
	{
		return view('reminders.duplicate', compact('reminder'))->with( 'lists', Reminder::getLists() );
	}



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Reminder  $reminder     * @return \Illuminate\Http\Response
     */
    public function update(Reminder $reminder, Request $request)
    {
        $this->validate_custom($request, $reminder);

        $input = $request->input('model');

        DB::beginTransaction();


		//update data
		$reminder->update( $input );

        //sync(attach/detach)
        if($request->input('pivots')){
            $this->sync($request->input('pivots'), $reminder);
        }

        DB::commit();

		return redirect()->route('reminders.index')->with('message', 'Item updated successfully.');
    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Reminder  $reminder     * @return \Illuminate\Http\Response
     */
    public function destroy(Reminder $reminder)
    {
		$reminder->delete();
        return back()->with('message', 'Item deleted successfully.');
		// return redirect()->route('reminders.index')->with('message', 'Item deleted successfully.');
    }

    /**
     * validate input data.
     *
     * @return array
     */
    public function validate_custom(Request $request, Reminder $reminder = null)
    {
        $request->validate(Reminder::getValidateRule($reminder));
    }

    /**
     * sync pivot data
     *
     * @return void
     */
    public function sync($pivots_data, Reminder $reminder)
    {
        foreach( $pivots_data as $pivot_child_model_name => $pivots ){

            // remove 'id'
            foreach($pivots as &$value){
                if( array_key_exists('id', $value) ){
                    unset($value['id']);
                }
            }unset($value);

            $method = camel_case( str_plural($pivot_child_model_name) );
            $reminder->$method()->sync($pivots);
        }
    }

}
