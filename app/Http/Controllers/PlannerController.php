<?php

namespace App\Http\Controllers;

use App\Planner;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\User;
// const USRID = 1;


class PlannerController extends Controller
{





    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, User $user)
    {

		$planners = new Planner;

        // (1)filltering
        if( is_array($request->input('q')) ){

            foreach( $request->input('q') as $key => $value ){

                if($key !== 's'){

                    $pettern = '#([^\.]*)\.?([^\.]*)_([^\._]*)#u';
                    preg_match($pettern,$key,$m);

                    $collumn_name = $m[1];
                    $related_column_name = $m[2];

                    if($m[3] == 'eq'){
                        $operator = '=';
                    }elseif($m[3] == 'cont'){
                        $operator = 'like';
                        $value = '%'.$value.'%';
                    }elseif($m[3] == 'gt'){
                        $operator = '>=';
                    }elseif($m[3] == 'lt'){
                        $operator = '<=';
                    }

                    if( $related_column_name !== '' ){  // search at related table column

                        $planners = $planners->whereHas($collumn_name, function($q) use($related_column_name, $operator, $value){
    						$q->where( $related_column_name, $operator, $value );
                        });

                    }else{
                        $planners = $planners->where( $collumn_name, $operator, $value );
                    }
                }
            }
        }
        $planners = auth()->user()->planners()->get();



        // (2)sort
        $q_s = $request->input('q.s');
        if($q_s){

            // sort dir and sort column
            if( substr( $q_s,-5,5 ) === '_desc' ){
                $sort_column = substr( $q_s, 0, strlen($q_s)-5 );
                $planners = $planners->sortByDesc($sort_column);
            }elseif( substr( $q_s,-4,4 ) === '_asc' ){
                $sort_column = substr( $q_s, 0, strlen($q_s)-4 );
                $planners = $planners->sortBy($sort_column);
            }

        }else{
            $planners = $planners->sortByDesc('id');
        }



        // (3)paginate
        $planners = $planners->paginate(10);

		return view('planners.index', compact('planners'));
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('planners.create')->with( 'lists', Planner::getLists() );
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate_custom($request);

        $input = $request->input('model');
        $input['user_id'] = auth()->user()->id;
        DB::beginTransaction();


		//create data
		$planner = Planner::create( $input );
        // $planner->user_id = auth()->user()->id;
        // dd($planner);
        //sync(attach/detach)
        if($request->input('pivots')){
            $this->sync($request->input('pivots'), $planner);
        }

        DB::commit();

		return redirect()->route('planners.index')->with('message', 'Item created successfully.');
    }



    /**
     * Display the specified resource.
     *
     * @param  \App\Planner  $planner     * @return \Illuminate\Http\Response
     */
    public function show(Planner $planner, User $user)
    {
        // dd(var_dump($this));
        if ($user->can('view', $planner)) {
          return view('planners.show', compact('planner'));
        } else {
          echo 'Not Authorized.';
        }
        // $this->authorize($planner);
        // return $this->makeResponse('planners.show', compact('planner'));
		// return view('planners.show', compact('planner'));
    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Planner  $planner     * @return \Illuminate\Http\Response
     */
    public function edit(Planner $planner)
    {
        $this->authorize($planner);
		return view('planners.edit', compact('planner'))->with( 'lists', Planner::getLists() );
    }



	/**
	 * Show the form for duplicatting the specified resource.
	 *
	 * @param \App\Planner  $planner	 * @return \Illuminate\Http\Response
	 */
	public function duplicate(Planner $planner)
	{
        $this->authorize($planner);
		return view('planners.duplicate', compact('planner'))->with( 'lists', Planner::getLists() );
	}



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Planner  $planner     * @return \Illuminate\Http\Response
     */
    public function update(Planner $planner, Request $request)
    {
        $this->authorize($planner);
        $this->validate_custom($request, $planner);

        $input = $request->input('model');

        DB::beginTransaction();


		//update data
		$planner->update( $input );

        //sync(attach/detach)
        if($request->input('pivots')){
            $this->sync($request->input('pivots'), $planner);
        }

        DB::commit();

		return redirect()->route('planners.index')->with('message', 'Item updated successfully.');
    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Planner  $planner     * @return \Illuminate\Http\Response
     */
    public function destroy(Planner $planner)
    {
        $this->authorize($planner);
		$planner->delete();
		return redirect()->route('planners.index')->with('message', 'Item deleted successfully.');
    }

    /**
     * validate input data.
     *
     * @return array
     */
    public function validate_custom(Request $request, Planner $planner = null)
    {
        $request->validate(Planner::getValidateRule($planner));
    }

    /**
     * sync pivot data
     *
     * @return void
     */
    public function sync($pivots_data, Planner $planner)
    {
        foreach( $pivots_data as $pivot_child_model_name => $pivots ){

            // remove 'id'
            foreach($pivots as &$value){
                if( array_key_exists('id', $value) ){
                    unset($value['id']);
                }
            }unset($value);

            $method = camel_case( str_plural($pivot_child_model_name) );
            $planner->$method()->sync($pivots);
        }
    }
}
