<?php

namespace App\Http\Controllers;

use App\Note;
use App\Planner;
use App\PlannerNote;
// use Illuminate\Http\Request;
use App\Notifications\UserCommented;

class PlannerNoteController extends Controller
{

    public function index(Planner $planner)
    {
        // $plannerNotes = $planner->notes;
        return view('plannerNotes.index', compact('planner'));
    }

    public function show(Planner $planner, Note $note)
    {
        // $planner = Planner::with('notes')->findOrFail($plannerId);
        // $note = $planner->notes()->findOrFail($noteId);
        $plannerId = $planner->id;
        return view('plannerNotes.show', compact('note','plannerId'));

    }

    public function edit($plannerId, $noteId)
    {
        $planner = Planner::with('notes')->findOrFail($plannerId);
        $note = $planner->notes()->findOrFail($noteId);
        return view('plannerNotes.edit', compact('note','plannerId'))->with( 'lists', Note::getLists() );
    }

    public function update(Planner $planner, $noteId)
    {
        // TODO: add validation
        $planner->updateNote(request('body'), $noteId);
        return redirect()->route('planners.notes.index', $planner->id)->with('message', 'Item updated successfully.');
    }

    public function create($plannerId)
    {
        return view('plannerNotes.create', compact('plannerId'))->with( 'lists', Note::getLists() );
    }

    public function store(Planner $planner)
    {
        // TODO: add validation
        $planner->addNote(request('body'));
        return redirect()->route('planners.notes.index', $planner->id)->with('message', 'Item created successfully.');
    }

    public function destroy($plannerId, Note $note)
    {
        $note->delete();
        return redirect()->route('planners.notes.index', $plannerId)->with('message', 'Item deleted successfully.');
    }







    // public function index(Request $request)
    // {
    //     $photo = Planner::with('notes')->findOrFail($request->route('planner'));

    //     return response()->json(['data' => $photo->notes]);
    // }

    // public function store(Request $request, Planner $photo)
    // {
    //     $data = $request->validate(['note' => 'required|string|between:2,500']);

    //     $comment = PlannerNote::create([
    //         'photo_id' => $photo->id,
    //         'comment' => $data['comment'],
    //         'user_id' => $request->user()->id,
    //     ]);

    //     if ($photo->user->allowsCommentsNotifications($request->user())) {
    //         $comment->notify(new UserCommented($request->user(), $photo, $comment));
    //     }

    //     return response()->json([
    //         'status' => 'success',
    //         'data' => $comment->load('user')
    //     ]);
    // }
}
