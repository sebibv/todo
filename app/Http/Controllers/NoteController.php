<?php

namespace App\Http\Controllers;

use App\Note;
use App\Planner;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class NoteController extends Controller
{
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
		$notes = new Note;

        // (1)filltering
        if( is_array($request->input('q')) ){

            foreach( $request->input('q') as $key => $value ){

                if($key !== 's'){

                    $pettern = '#([^\.]*)\.?([^\.]*)_([^\._]*)#u';
                    preg_match($pettern,$key,$m);

                    $collumn_name = $m[1];
                    $related_column_name = $m[2];

                    if($m[3] == 'eq'){
                        $operator = '=';
                    }elseif($m[3] == 'cont'){
                        $operator = 'like';
                        $value = '%'.$value.'%';
                    }elseif($m[3] == 'gt'){
                        $operator = '>=';
                    }elseif($m[3] == 'lt'){
                        $operator = '<=';
                    }

                    if( $related_column_name !== '' ){  // search at related table column

                        $notes = $notes->whereHas($collumn_name, function($q) use($related_column_name, $operator, $value){
    						$q->where( $related_column_name, $operator, $value );
                        });

                    }else{
                        $notes = $notes->where( $collumn_name, $operator, $value );
                    }
                }
            }
        }
        $notes = $notes->get();



        // (2)sort
        $q_s = $request->input('q.s');
        if($q_s){

            // sort dir and sort column
            if( substr( $q_s,-5,5 ) === '_desc' ){
                $sort_column = substr( $q_s, 0, strlen($q_s)-5 );
                $notes = $notes->sortByDesc($sort_column);
            }elseif( substr( $q_s,-4,4 ) === '_asc' ){
                $sort_column = substr( $q_s, 0, strlen($q_s)-4 );
                $notes = $notes->sortBy($sort_column);
            }

        }else{
            $notes = $notes->sortByDesc('id');
        }



        // (3)paginate
        $notes = $notes->paginate(10);

		return view('notes.index', compact('notes'));
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('notes.create')->with( 'lists', Note::getLists() );
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return "haha";
  //       $this->validate_custom($request);

  //       $input = $request->input('model');

  //       DB::beginTransaction();


		// //create data
		// $note = Note::create( $input );

  //       //sync(attach/detach)
  //       if($request->input('pivots')){
  //           $this->sync($request->input('pivots'), $note);
  //       }

  //       DB::commit();

		// return redirect()->route('notes.index')->with('message', 'Item created successfully.');
    }



    /**
     * Display the specified resource.
     *
     * @param  \App\Note  $note     * @return \Illuminate\Http\Response
     */
    public function show(Note $note)
    {
		return view('notes.show', compact('note'));
    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Note  $note     * @return \Illuminate\Http\Response
     */
    public function edit(Note $note)
    {
		return view('notes.edit', compact('note'))->with( 'lists', Note::getLists() );
    }



	/**
	 * Show the form for duplicatting the specified resource.
	 *
	 * @param \App\Note  $note	 * @return \Illuminate\Http\Response
	 */
	public function duplicate(Note $note)
	{
		return view('notes.duplicate', compact('note'))->with( 'lists', Note::getLists() );
	}



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Note  $note     * @return \Illuminate\Http\Response
     */
    public function update(Note $note, Request $request)
    {
        $this->validate_custom($request, $note);

        $input = $request->input('model');

        DB::beginTransaction();


		//update data
		$note->update( $input );

        //sync(attach/detach)
        if($request->input('pivots')){
            $this->sync($request->input('pivots'), $note);
        }

        DB::commit();

		return redirect()->route('notes.index')->with('message', 'Item updated successfully.');
    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Note  $note     * @return \Illuminate\Http\Response
     */
    public function destroy(Note $note)
    {
		$note->delete();
		return redirect()->route('notes.index')->with('message', 'Item deleted successfully.');
    }

    /**
     * validate input data.
     *
     * @return array
     */
    public function validate_custom(Request $request, Note $note = null)
    {
        $request->validate(Note::getValidateRule($note));
    }

    /**
     * sync pivot data
     *
     * @return void
     */
    public function sync($pivots_data, Note $note)
    {
        foreach( $pivots_data as $pivot_child_model_name => $pivots ){

            // remove 'id'
            foreach($pivots as &$value){
                if( array_key_exists('id', $value) ){
                    unset($value['id']);
                }
            }unset($value);

            $method = camel_case( str_plural($pivot_child_model_name) );
            $note->$method()->sync($pivots);
        }
    }
}
