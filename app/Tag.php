<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Tag extends Model
{


	// Mass Assignment
	protected $fillable = ['planner_id','tag',];


	// Validate Rule
    public static function getValidateRule(Tag $tag=null){
        if($tag){
            $ignore_unique = $tag->id;
        }else{
            $ignore_unique = 'NULL';
        }
        $table_name = 'tags';
        $validation_rule = [

            'model.planner_id' => 'integer|nullable',
            'model.tag' => 'string',


        ];
        if($tag){

        }
        return $validation_rule;
    }



	public function planner() {
		return $this->belongsTo('App\Planner');
	}




	public static function getLists() {
		$lists = [];
		$lists['Planner'] = Planner::pluck( 'name' ,'id' );
		return $lists;
	}
}
