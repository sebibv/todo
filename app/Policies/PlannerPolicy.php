<?php

namespace App\Policies;

use App\User;
use App\Planner;
use Auth;
use Illuminate\Auth\Access\HandlesAuthorization;
// use Illuminate\Support\Facades\Auth;


class PlannerPolicy
{

    use HandlesAuthorization;
    // Debugbar::info($user);
    // var_dump($user);
    // print("Hello World");
    /**
     * Determine whether the user can view the planner.
     *
     * @param  \App\User  $user
     * @param  \App\Planner  $planner
     * @return mixed
     */
    public function view(User $user, Planner $planner)
    {
        // var_dump($user->id); //null

        // $planner->user_id can be a string because of PDO, add to Model
        // protected $casts = [
        // 'user_id' => 'integer'
        // ];
        return Auth::id() === $planner->user_id;
        // return $user->id === $planner->user_id;
    }

    // public function show(User $user, Planner $planner)
    // {
    //     return $user->id === $planner->user_id;
    // }
    /**
     * Determine whether the user can create planners.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return Auth::id() === $planner->user_id;
    }

    /**
     * Determine whether the user can update the planner.
     *
     * @param  \App\User  $user
     * @param  \App\Planner  $planner
     * @return mixed
     */
    public function update(User $user, Planner $planner)
    {
        return Auth::id() === $planner->user_id;
    }

    /**
     * Determine whether the user can delete the planner.
     *
     * @param  \App\User  $user
     * @param  \App\Planner  $planner
     * @return mixed
     */
    public function delete(User $user, Planner $planner)
    {
        return Auth::id() === $planner->user_id;
    }
}
