<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;



class User extends Authenticatable
{
    use Notifiable;





    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','timezone',    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


	// Validate Rule
    public static function getValidateRule(User $user=null){
        if($user){
            $ignore_unique = $user->id;
        }else{
            $ignore_unique = 'NULL';
        }
        $table_name = 'users';
        $validation_rule = [

            'model.name' => 'required',
            'model.email' => 'required|unique:'.$table_name.',email,'.$ignore_unique.',id,deleted_at,NOT_NULL',
            'model.password' => 'required',


        ];
        if($user){

        }
        return $validation_rule;
    }

	public function planners() {
		return $this->hasMany('App\Planner');
	}


	public static function getLists() {
		$lists = [];
		$lists['Planner'] = Planner::pluck( 'name' ,'id' );
		return $lists;
	}
}
