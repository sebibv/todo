<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Note extends Model
{


	// Mass Assignment
	protected $fillable = ['planner_id','note',];


	// Validate Rule
    public static function getValidateRule(Note $note=null){
        if($note){
            $ignore_unique = $note->id;
        }else{
            $ignore_unique = 'NULL';
        }
        $table_name = 'notes';
        $validation_rule = [

            'model.planner_id' => 'integer|nullable',
            'model.note' => 'string',


        ];
        if($note){

        }
        return $validation_rule;
    }

	public function reminders() {
		return $this->hasMany('App\Reminder');
	}


	public function planner() {
		return $this->belongsTo('App\Planner');
	}




	public static function getLists() {
		$lists = [];
		$lists['Planner'] = Planner::pluck( 'name' ,'id' );
		$lists['Reminder'] = Reminder::pluck( 'when' ,'id' );
		return $lists;
	}
}
