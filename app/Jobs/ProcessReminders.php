<?php

namespace App\Jobs;


use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Reminder;

class ProcessReminders implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */

    // TODO: de schimbat ShouldQueue pe driver, in .env
    public function handle()
    {
        $now = now();
        $reminders_to_process =
            Reminder::whereNull('deleted_at')
            // where id se inlocuieste cu whereNull('notified_at')
            // ->where('id', '>', 0)
            ->whereNull('notified_at')
            ->where('when', '<=',  $now)->get();

        $text_accumulator = "";
        foreach($reminders_to_process as $reminder_to_process) {
            $text_accumulator .= $reminder_to_process->when->format('Y-M-d D H:i') . ' UTC: ' . $reminder_to_process->note->note . " || Notificat la: " .  $now . " UTC\n";
        }
        $successBool = \Storage::prepend('public/test.txt', $text_accumulator);

        // checks if the write is a success then set reminders as notified
        if($successBool) {
            Reminder::whereIn('id',$reminders_to_process->pluck('id'))
                ->update(['notified_at'=>$now]);
        }
        // TODO: if error
        \Log::info("test.txt write: " . $successBool);


        // TODO: check if asset link exist, only add if not
        asset('storage/test.txt');
    }
}
